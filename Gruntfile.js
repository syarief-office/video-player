module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            basic_and_extras: {
              files: {
                'view/assets/js/libs/libs.js': [
                    'source/js/libs/jquery-1.11.0.min.js',
                    'source/js/libs/handlebars-v2.0.0.js',
                ],
                'view/assets/js/script.js': ['source/js/script.js'],
                //'view/assets/js/app/app.js': ['source/js/app/*.js' ]
                //'view/assets/js/libs/html5shiv.js' : 'source/js/libs/html5shiv.js', 
                'view/assets/js/libs/modernizr-2.6.2.min.js' : 'source/js/libs/modernizr-2.6.2.min.js'
              },
            },
        },

        //COMPILING COMPASS
        compass: {                  // Task
            dist: {                   // Target
              options: {              // Target options
                sassDir: 'source/sass',
                cssDir: 'view/assets/css',
               // config: 'config.rb'
              }
            }
        },

        /*COMPILING SASS*/
        sass: {                              
            dist: {                            
              options: {                       
                style: 'expanded'
              },
              files: {                         
                'view/assets/css/style.css': 'source/sass/style.scss',
                'view/assets/partials/bootstrap/bootstrap.css': 'source/sass/partials/bootstrap/bootstrap.scss'

                //'view/assets/css/libs/libs.css': ['source/sass/']
              }
            }
          },

        /*COMPILING IMAGE*/
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'source/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'view/assets/img'
                }]
            }
        },


        /*HANDLEBARS */ 
        handlebars: {
          compile: {
            options: {
              namespace: "JST"
            },
            files: {
              "view/assets/js/handlebars/template.js": "source/handlebars/*.hbs"
              //"path/to/another.js": ["path/to/sources/*.hbs", "path/to/more/*.hbs"]
            }
          }
        },

        //REFORMATING HTML
        prettify: {
            options: {
              "indent": 4,
                "condense": true
            },
            html: {
                expand: true,
                cwd: 'source/',
                ext: '.html',
                src: ['*.html'],
                dest: 'view/'
 
            }
          },

        //COMPAIL ALL FILES
        watch: {
            options: {
                livereload: 3902
            },
            
            scripts: {
                files: ['source/js/*.js'],
                tasks: ['concat'],
                options: {
                    spawn: false,
                },
            }, 

           comapssass:{
                    files: ['source/sass/*.scss'],
                    tasks: ['compass'],
                    options: {
                        spawn: false,
                    }
            },

            css:{
                    files: ['source/sass/*.scss'],
                    tasks: ['sass'],
                    options: {
                        spawn: false,
                    }
            },

            hbs:{
                files: ['source/handlebars/*.hbs'],
                tasks: ['handlebars'],
                options: {
                    spawn: false,
                }
            },

            html:{
                files: ['source/*.html'],
                tasks: ['prettify'],
                options: {
                    spawn: false,
                }
            }
        }

    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-prettify');

    grunt.registerTask('default', ['concat', 'sass', 'compass', 'imagemin', 'watch', 'handlebars', 'prettify']);

};