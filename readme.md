# **Video playe**
***
## **Front-End Development**

**File Structure**

 - ***`videoplayer.git`***
	 - **`output`**
	 - **`source`**
		 - `assets`
			 - `fonts`
			 - `icons`
			 - `images`
			 - `scripts`
			 - `styles`
		 - `libs`
		 - `tpl`
			 - `data`
			 - `layout`
				 - `*.swig`
			 - `module`
			 - `*.swig`

#### **Installation**
Please clone this repo. This project is using Swig for template engine. Please do editing only inside **`source/`** folder.

#### **Install NodeJS**
Download from this link and install NodeJS.
>http://nodejs.org/download/

#### **Install Grunt**
Open Console and run this command:

> **`npm install -g grunt-cli`** *atau* **`sudo npm install -g grunt-cli`**

#### **Install NodeJS Modules**
Install `node_modules` for compiling project. Open console, `cd` to the project path. E.g: **`/Users/username/project/cloudrfp-v4.git`**.

> **`npm install`** *atau* **`sudo npm install`**.

Now you're ready to compile the project.

#### **Compile**
Open console, `cd` to the project path and run this command:

>**`grunt`**

If success, then the compiler will return something like this:

```
Running "watch" task
Waiting...
```

#### ***Notes***
Livereload is included in the grunt. So, while you save changes on the source, the browser will reload automaticaly. 
